\section{Binary Search in One Dimension}

Our major tool is the classical binary search over one-dimensional and totally-ordered domains, where a partition of $[0,1]$ is of the form $M=([0,z),[z,1])$ for some $0<z<1$. The outcome of the search procedure is a pair of numbers $\uy$ and $\oy$ such that $\uy<z<\oy$, which implies a partition approximation $M'=([0,\uy),[\oy,1])$. The quality of $M'$ is measured by the size of the gap $\oy-\uy$, which can be made as small as needed by running more steps. Note that in one dimension, $\oy-\uy$ is both the volume of $[\uy,\oy]$ and its diameter. We are going to apply binary search to straight lines of arbitrary position and arbitrary positive orientation inside high-dimensional $X$, hence we formulate it in terms that will facilitate its application in this context.


\begin{definition}[Line Segments in High-Dimension]
The line segment connecting two points  $\ux<\ox\in X =[0,1]^n$ is their
convex hull
$$\diag{\ux}{\ox}=\{(1-\l)\ux+\l \ox:\l \in [0,1]\}.$$
The segment inherits a total order from $[0,1]$: $x\leq x'$ whenever $\l\leq \l'$.
\end{definition}

The input to the binary search procedure, written in Algorithm~\ref{alg:binary}, is a line segment $\ell$ and an oracle for a monotone partition $M=(\ull,\oll)=(\diag{\ux}{z},\diag{z}{\ox})$,  $\ux<z<\ox$. The output is a sub-segment $\diag{\uy}{\oy}$ containing the boundary point $z$. The procedure is parameterized by an error bound $\eps\geq 0$, with $\eps=0$ representing an ideal variant of the algorithm that runs indefinitely and finds the exact boundary point. Although realizable only in the limit, it is sometimes convenient to speak in terms of this variant. Fig.~\ref{fig:bsearch-no-tree} illustrates several steps of the algorithm. 


\comment{When $\eps>0$, and $\{\diag{\ux}{\uy},\diag{\uy}{\oy},\diag{\oy}{\ox}\}$ is a partition of $\ell$.}

\comment{
Binary search can be viewed also as selecting a path in a binary tree whose nodes are labeled by intervals, with the root labeled by $X$ and the sons of each interval $[a,b]$ are the intervals $[a,(a+b)/2]$ and $[(a+b)/2, b]$.}


\begin{algorithm}
  \caption{One dimensional binary search: $search(\diag{\ux}{\ox},\eps$)}
  \label{alg:binary}
  \begin{algorithmic}[1]
    \State {{\bf Input}:  A line segment $\ell=\diag{\ux}{\ox}$, a monotone partition $M=(\ull,\oll)$ accessible via an oracle $member()$ for membership in $\oll$ and an error bound $\eps\geq 0$.}
     \State {{\bf Output}:   %An approximation of $M$,  $M'=\{\diag{\ux}{\uy},\diag{\oy}{\ox}\}$
     A line segment $\diag{\oy}{\uy}$  containing $bd(M)$ such that $\oy-\uy\leq\eps$.}
          \smallskip
     %     \State{$(\uq,\oq)=(member(\ux),member(\ox))$}
        %  \If {$(\uq,\oq)=(0,0)$}
        % \Return {$M_0$}
         % \ElsIf {$(\uq,\oq)=(1,1)$}
         % \Return {$M_1$}
         % \Else \Comment{non-degenerate case}
       \State {$\diag{\uy}{\oy}=\diag{\ux}{\ox}$}
    \While {$\oy-\uy\geq\eps$}
        \State {$y=(\uy+\oy)/2$}
      \If {$member(y)$}
          \State   {$\diag{\uy}{\oy}=\diag{\uy}{y}$} \Comment{left sub-interval}
      \Else
              \State   {$\diag{\uy}{\oy}=\diag{y}{\oy}$} \Comment{right sub-interval}
      \EndIf
          \EndWhile
          \State{\Return {$\diag{\uy}{\oy}$}}
    %  \EndIf
  \end{algorithmic}
\end{algorithm}
%



\begin{figure}[h]
  \centering
\scalebox{0.85}{\input bsearch-no-tree.PDFTEX_T }
  \caption{Binary search and the successive reduction of the uncertainty interval. \comment{The rightmost known element of $\uX$ and leftmost known elements of $\oX$ are indicated by $-$ and $+$, respectively. On the right we see the same process interpreted as path selection in a binary tree.} }\label{fig:bsearch-no-tree}
\end{figure}

 \comment{
 In Section~\ref{sec:high-dim} we  present some basic notions and facts concerning domination and incomparability between point and sets in partially-ordered domains. These are important for analyzing the complexity and limitation of our algorithm and for suggesting, eventually, some improvements. In Section~\ref{sec:algorithm} we describe the algorithm, first in an abstract manner and then provide more implementation details which are important for its robust operation.
 }