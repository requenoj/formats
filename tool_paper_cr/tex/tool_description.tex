\section{ParetoLib Library}
\label{sec:paretolib}
\subsection{Main features}
ParetoLib is a Python library that is free and publicly available on Internet 
\cite{paretolib}.
It has been implemented following the software engineering standards, suggestions and best practices for this language, including a brief documentation and a set of illustrative examples of how to use it.
The library is compatible with Python 2.7, 3.4 or higher; and it is PEP 8 compliant.
The code has been exhaustively tested, reaching a coverage of more than 90\% of the code in the module devoted to the sequential approximation of the monotone partitions and boundary.
It also supports multi-core CPUs in order to take advantage of concurrent processing.
ParetoLib implements the original discovery algorithm presented in \cite{maler:hal-01556243,vazquez_github} as well as a new smarter version shown in \cite{bakhirkin_basset_maler_requeno:hal}.
The algorithm introduced in \cite{bakhirkin_basset_maler_requeno:hal} outperforms the previous one in inferring the Pareto front of multi-dimensional spaces when the number of dimensions is higher than $3$. 
The new method still suffers from the curse of dimensionality, but it can be used to extract information for high dimensional models by combinining solutions from several low-dimensional sub-problems (see \cite{DBLP:conf/rv/Vazquez-Chanlatte18,DBLP:conf/cav/Vazquez-Chanlatte17} for a similar approach). 
% (i.e., the main algorithm processes nproc boxes from the boundary in parallel at each iteration step, being nproc the number of processing units).
%A preliminary version of our Pareto front discovery algorithm was implemented in \cite{vazquez_github} and demonstrated the usefulness in \cite{DBLP:conf/cav/Vazquez-Chanlatte17,DBLP:conf/rv/Vazquez-Chanlatte18}, but that tool did not include all the recent refinements and optimisations in algorithm convergence and multiprocessing yet.

The design of ParetoLib separates the interface to the external decision 
procedure from the implementation of the multi-dimensional search (Fig.~\ref{fig:pareto-diagram}).
The implementation of multi-dimensional search is unique, and it is responsible 
for constructing the approximation of the Pareto front, based on which parameter 
valuations make the predicate valid and which do not.
Parameter valuations are treated abstractly, as points in multi-dimensional 
space, disregarding their positions and roles in the predicate.

\begin{figure}
\centering
\includegraphics[width = 0.95\linewidth]{pictures/paretolibtool/paretolibtool_detailed.png}
\caption{Diagram showing the interaction between the tool components. }
\label{fig:pareto-diagram}
\end{figure}

An adaptor to an external decision procedure, named oracle,
is responsible for getting from the user a parameterized predicate, 
extracting from it the set of parameters, and, given a parameter valuation, 
instantiating the parameters and sending the resulting query to the decision 
procedure.
It is also responsible for relaying extra inputs to the decision procedure when 
necessary.
For example, when using ParetoLib for STL parameter synthesis, the oracle is 
responsible for relaying the input trace(s) to the runtime monitor.
ParetoLib allows the users to implement their own oracles and has several built 
in:
\begin{compactitem}
 \item Two oracles that interface with STL runtime monitors: 
  \textit{OracleSTL} interacts with AMT 2.0 \cite{amt}, and
  \textit{OracleSTLe} interacts with StlEval \cite{stleval-gitlab}.
 These oracles are used to perform STL parameter synthesis.
 
 \item Two oracles that interface with built-in procedures: 
 \textit{OracleFunction} checks whether a point satisfies a set of polynomial constraints, and
 \textit{OraclePoint} stores a cloud of pre-defined Pareto points in a NDTree \cite{ndtree}.
 They may be used for test purposes.
 % and ``proof of concept''.
\end{compactitem}

%TODO: This has to go elsewhere, perhaps to the next section, as an example of 
%output.
%
%Image \ref{fig:example} shows a set of discrete points that outlines the Pareto front, and the partitioning that is learnt by our algorithm thanks to the oracle guidance.
%The green side represents the upper closure and the red side represents the lower closure.
%A gap in blue may appear between the two closures, which is the border and can be set arbitrarily small depending on the accuracy required by the user.
%
%\begin{figure}[h!]
%\centering
%\subfloat[]{\includegraphics[width = 0.3\linewidth]{pictures/pareto_front.jpg}}
%\hspace{0.01\linewidth}
%\subfloat[]{\includegraphics[width = 
%0.3\linewidth]{pictures/multidim_search.jpg}}
%\caption{Original Pareto front (a) and the inferred closures (b) learnt by our partitioning algorithm}
%\label{fig:example}
%\end{figure}

\subsection{Interaction with Signal Temporal Logic}

% In the experiments with STL parameter synthesis we use the tool StlEval 
%\cite{stleval-gitlab} to as the runtime monitor (i.e., ParetoLib has a built-in 
%oracle that interfaces with StlEval).
The StlEval \cite{stleval-gitlab} tool is selected as the STL runtime monitor for running the experiments of parameter synthesis for STL formulas in ParetoLib (Section~\ref{sec:experiments}).
StlEval allows to evaluate formulas in an extension of STL \cite{alexey2019} 
over signals with piecewise-constant interpolation. 
It also allows to compute robustness \cite{donze10robustness,thomas13robustness} 
of STL formulas. %, but this is not relevant for ParetoLib.
Its specification language supports a number of operations:
\begin{compactitem}
  \item standard STL operators: ``always'', ``eventually'', ``until'', etc;
  \item pre-processing operations: scaling, adding, subtracting, shifting 
  signals, etc;
  \item computing minimum and maximum of a signal over a sliding window;
\end{compactitem}
For example, the formula:
%\texttt{(<= (On (0 10) (- (Max x0) (Min x0))) 0.1)}
\texttt{(<= (On (0 10) (- (Max x) (Min x))) 0.1)}
asserts that the difference between the maximum and minimum of the signal 
%\texttt{x0} on the interval from now to 10 time points into the future is not 
$x$ on the interval from now to 10 time points into the future is not 
greater than $0.1$.
Although the particulars of the specification language are not important for 
ParetoLib, in the experiments our specifications use some features that are 
unique to StlEval and are not present in AMT 2.0.
StlEval also provides several interfaces to the users:
\begin{compactitem}
  \item Command line interface that allows to read a signal and evaluate one or 
  more formulas over it.
  \item Interactive command interface that allows to send commands to a running 
  instance of StlEval via standard input and get results via standard output 
  (similarly to how some SMT solvers implement SMTLIB command language).
  \item C++ API via a static or shared library with access to all functions and 
  data structures.
  \item C API via a static or shared library with access to select functions
  and data structures.
\end{compactitem}
ParetoLib can use either the interactive command interface or the C API, the 
latter being marginally more efficient.

